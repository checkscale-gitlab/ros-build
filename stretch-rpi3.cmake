set(CMAKE_CROSSCOMPILING TRUE)
set(CMAKE_LIBRARY_ARCHITECTURE arm-linux-gnueabihf)

set(CMAKE_C_COMPILER /usr/bin/arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER /usr/bin/arm-linux-gnueabihf-g++)

set(__MYFLAGS "-march=armv6 -marm -mfpu=vfp")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${__MYFLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${__MYFLAGS}")

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Xlinker -rpath-link=/opt/ros/$ENV{ROS_DISTRO}/lib")

## CFLAGS="-march=armv8-a+crc -mtune=cortex-a53 -mfpu=crypto-neon-fp-armv8 -mfloat-abi=hard -ftree-vectorize -funsafe-math-optimizations -O2 -pipe"

#set(CMAKE_SYSROOT ${CHROOT})
#set(CMAKE_FIND_ROOT_PATH ${CMAKE_SYSROOT})
#set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)
