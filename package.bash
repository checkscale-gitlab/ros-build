#!/bin/bash
set -e

# Before package build

ln -s /tmp "/opt/ros/$ROS_DISTRO/share/flexbe_onboard/tmp"


# Build Debian package

source "buildpkg.bash"

DEB_NAME="ros-${ROS_DISTRO}-sweetie-bot-base$PKG_SUFFIX"

mkdir package
pushd package
PUBLISHER="/opt/ros/$ROS_DISTRO/lib/joint_state_publisher/joint_state_publisher"
PUBLISHER_DIVERT="0.1-396~$(lsb_release -sc)"
if false; then # if dpkg -S "$PUBLISHER"; then
    cat >preinst <<EOF
if [ upgrade != "\$1" ] || dpkg --compare-versions "\$2" lt "$PUBLISHER_DIVERT"
then
    dpkg-divert --package "$DEB_NAME" --add --rename --divert "$PUBLISHER.real" "$PUBLISHER"
fi
EOF
    cat >postrm <<EOF
if [ remove = "\$1" -o abort-install = "\$1" -o disappear = "\$1" ] || \
   [ abort-upgrade = "\$1" ] && dpkg --compare-versions "\$2" lt "$PUBLISHER_DIVERT"
then
    [ -f "$PUBLISHER" ] && rm "$PUBLISHER"
    dpkg-divert --package "$DEB_NAME" --remove --rename --divert "$PUBLISHER.real" "$PUBLISHER"
fi
EOF
    cat >postinst <<OUTER_EOF
#!/bin/bash
set -e
cp "$PUBLISHER.real" "$PUBLISHER"
cd "/opt/ros/$ROS_DISTRO/lib/joint_state_publisher"
patch -Np1 <<EOF
$( cat ../joint_state_publisher.patch )
EOF
OUTER_EOF
    chmod +x preinst postrm postinst
fi

touch "$DEPS_FILE" "$DEPS_FILE.exclude"
for p in $(cat "$DEPS_FILE");         do echo $p; done | sort -u > deps
for p in $(cat "$DEPS_FILE.exclude"); do echo $p; done | sort -u > deps.exclude
DEPS="$(diff -u deps deps.exclude  | grep ^-\\w | sed -e 's/^-//')"
DEB_DESCRIPTION="Sweetie Bot Project base package for $ROS_DISTRO"
DEB_CONFLICTS="ros-${ROS_DISTRO}-orogen ros-${ROS_DISTRO}-ocl ros-${ROS_DISTRO}-rtt ros-${ROS_DISTRO}-log4cpp $(cat deps.exclude)"
rm deps.exclude deps
buildpkg "/opt" "sweetie-bot-base.deb" $DEPS
mv "sweetie-bot-base.deb" ..
popd
